const AWS = require('aws-sdk');
const { join } = require('path');

const Bucket = 'buckets.email'
const domain = 'luddites.co'
const user = 'ned'

const s3 = new AWS.S3()

/**
 * Lambda handler for SES events.
 * @param {!_mailroom.event} event The event.
 * @param {*} context
 * @param {*} callback
 */
async function handler(event, context, callback) {
  const { ses: { receipt, mail: { messageId }, mail }, ses } = event.Records[0]
  // console.log("SES Notification:\n", JSON.stringify(sesNotification, null, 2))

  let FOLDER = 'index'

  // Check if any spam check failed
  if (receipt.spfVerdict.status == 'FAIL'
      || receipt.dkimVerdict.status == 'FAIL'
      || receipt.spamVerdict.status == 'FAIL'
      || receipt.virusVerdict.status == 'FAIL') {
    // console.log('Dropping spam')
    FOLDER = 'spam'
    // Stop processing rule set, dropping message
    // return callback(null, {'disposition':'STOP_RULE_SET'});
  }

  const dir = join(domain, user)

  let { timestamp, source, commonHeaders: { subject = '-', from = source } } = mail
  subject = subject.substr(0, 100)
  Array.isArray(from) && (from = from.join(', '))
  from = from.substr(0, 100).replace(/"/g, '\\"')

  const D = 10000000000000
  const dd = (D - new Date(timestamp).getTime()) / 1000
  const meta = `${dd} ${messageId} "${from}" ${subject}`

  const Key = join(dir, FOLDER, meta)

  await s3.putObject({
    Bucket,
    Key,
    Body: JSON.stringify(ses),
  }).promise()

  callback(null, null)
}

/**
 * @typedef {import('../types').event} _mailroom.event
 */

// Retrieve the email from your bucket
// const { Body, LastModified } = await s3.getObject({
//     Key,
//     Bucket,
// }).promise()

module.exports.handler = handler