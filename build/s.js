var AWS = require('aws-sdk')
var s3 = new AWS.S3()
const { join } = require('path')

const Bucket = 'buckets.email'
const domain = 'luddites.co'
const user = 'ned'

exports.handler = async function(event, context, callback) {
    const { ses: { receipt, mail: { messageId }, mail }, ses } = event.Records[0]
    // console.log("SES Notification:\n", JSON.stringify(sesNotification, null, 2))

    let FOLDER = 'index'

     // Check if any spam check failed
    if (receipt.spfVerdict.status == 'FAIL'
        || receipt.dkimVerdict.status == 'FAIL'
        || receipt.spamVerdict.status == 'FAIL'
        || receipt.virusVerdict.status == 'FAIL') {
        // console.log('Dropping spam')
        FOLDER = 'spam'
        // Stop processing rule set, dropping message
        // return callback(null, {'disposition':'STOP_RULE_SET'});
    }

    // console.log('Processing message:', messageId);

    // // If DMARC verdict is FAIL and the sending domain's policy is REJECT
    // // (p=reject), bounce the email.
    // if (receipt.dmarcVerdict.status === 'FAIL' && receipt.dmarcPolicy.status === 'REJECT') {
    //     // The values that make up the body of the bounce message.
    //     const sendBounceParams = {
    //         BounceSender: `mailer-daemon@${emailDomain}`,
    //         OriginalMessageId: messageId,
    //         MessageDsn: {
    //             ReportingMta: `dns; ${emailDomain}`,
    //             ArrivalDate: new Date(),
    //             ExtensionFields: [],
    //         },
    //         // Include custom text explaining why the email was bounced.
    //         Explanation: "Unauthenticated email is not accepted due to the sending domain's DMARC policy.",
    //         BouncedRecipientInfoList: receipt.recipients.map((recipient) => ({
    //             Recipient: recipient,
    //             // Bounce with 550 5.6.1 Message content rejected
    //             BounceType: 'ContentRejected',
    //         })),
    //     }

    //     // console.log('Bouncing message with parameters:');
    //     // console.log(JSON.stringify(sendBounceParams, null, 2));
    //     // Try to send the bounce.
    //     new AWS.SES().sendBounce(sendBounceParams, (err, data) => {
    //         // If something goes wrong, log the issue.
    //         if (err) {
    //             console.log(`An error occurred while sending bounce for message: ${messageId}`, err);
    //             callback(err);
    //         // Otherwise, log the message ID for the bounce email.
    //         } else {
    //             console.log(`Bounce for message ${messageId} sent, bounce message ID: ${data.MessageId}`);
    //             // Stop processing additional receipt rules in the rule set.
    //             callback(null, {
    //                 disposition: 'stop_rule_set',
    //             });
    //         }
    //     });
    // }
    // If the DMARC verdict is anything else (PASS, QUARANTINE or GRAY), accept
    // the message and process remaining receipt rules in the rule set.

    const dir = join(domain, user)
    // const Key = join(dir, messageId)
    let { timestamp, source, commonHeaders: { subject = '-', from = source } } = mail
    subject = subject.substr(0, 100)
    source = source.substr(0, 100).replace(/"/g, '\\"')

    // Retrieve the email from your bucket
    // const { Body, LastModified } = await s3.getObject({
    //     Key,
    //     Bucket,
    // }).promise()

    const D = 10000000000000
    const dd = (D - new Date(timestamp).getTime()) / 1000
    const meta = `${dd} ${messageId} "${source}" ${subject}`

    const Key = join(dir, FOLDER, meta)
    console.log(Key)

    await s3.putObject({
        Bucket,
        Key,
        Body: JSON.stringify(ses),
    }).promise()
}