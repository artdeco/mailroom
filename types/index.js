export {}
/* typal types/index.xml namespace */
/**
 * @typedef {import('aws-sdk').S3} _aws.S3
 * @typedef {import('@idio/idio').Context} _idio.Context
 * @typedef {_mailroom.Message} Message `＠record` A message record in S3.
 * @typedef {Object} _mailroom.Message `＠record` A message record in S3.
 * @prop {string} Key
 * @prop {!Date} LastModified
 * @prop {number} ContentLength
 * @typedef {_mailroom.Context} Context `＠record` A context for the middleware.
 * @typedef {_mailroom.$Context & _idio.Context} _mailroom.Context `＠record` A context for the middleware.
 * @typedef {Object} _mailroom.$Context `＠record` A context for the middleware.
 * @prop {string} Bucket
 * @prop {string} Prefix
 * @prop {_aws.S3} s3
 * @typedef {_mailroom.Middleware} Middleware The middleware.
 * @typedef {(ctx: _mailroom.Context) => Promise} _mailroom.Middleware The middleware.
 */

/* typal types/event.xml namespace */
/**
 * @typedef {_mailroom.event} event `＠record` An event for Lambda function from SES.
 * @typedef {Object} _mailroom.event `＠record` An event for Lambda function from SES.
 * @prop {!Array<!_mailroom.Record>} Records
 * @typedef {_mailroom.Record} Record `＠record`
 * @typedef {Object} _mailroom.Record `＠record`
 * @prop {!_mailroom.SES} ses
 * @typedef {_mailroom.SES} SES `＠record`
 * @typedef {Object} _mailroom.SES `＠record`
 * @prop {!_mailroom.Mail} mail Message metadata.
 * @prop {!_mailroom.Receipt} receipt Processing metadata.
 * @typedef {_mailroom.Mail} Mail `＠record` Info about the email message.
 * @typedef {Object} _mailroom.Mail `＠record` Info about the email message.
 * @prop {string} timestamp When the message was received (in ISO8601 format),
 * e.g., `2019-08-05T21:30:02.028Z`.
 * @prop {string} source The email address of the message sender,
 * e.g., `ned@ludd.io`.
 * @prop {string} messageId A message ID assigned by Amazon SES,
 * e.g.,`o3vrnil0e2ic28trm7dfhrc2v0clambda4nbp0g1`.
 * @prop {!Array<string>} destination Who should receive the message, e.g., `[recipient@example.com]`.
 * @prop {boolean} headersTruncated Whether not all headers were included in the event.
 * @prop {!_mailroom.CommonHeaders} commonHeaders
 * @typedef {_mailroom.CommonHeaders} CommonHeaders `＠record`
 * @typedef {Object} _mailroom.CommonHeaders `＠record`
 * @prop {string} returnPath Where to return the message to.
 * @prop {!Array<string>} from The `From` header(s).
 * @prop {string} date The `Date` header.
 * @prop {!Array<string>} to The to header.
 * @prop {string} messageId The sender's message id.
 * @prop {string} subject The `Subject` header.
 * @typedef {_mailroom.Receipt} Receipt `＠record`
 * @typedef {Object} _mailroom.Receipt `＠record`
 * @prop {string} timestamp When the message was processes (in ISO8601 format),
 * e.g., `2019-08-05T21:30:02.028Z`.
 * @prop {number} processingTimeMillis How long it took to process the message.
 * @prop {!Array<string>} recipients Who received the message.
 * @prop {!_mailroom.Verdict} spamVerdict
 * @prop {!_mailroom.Verdict} virusVerdict
 * @prop {!_mailroom.Verdict} spfVerdict
 * @prop {!_mailroom.Verdict} dkimVerdict
 * @prop {!_mailroom.Verdict} dmarcVerdict
 * @typedef {_mailroom.Verdict} Verdict `＠record` The SES decision about viruses, spam, _etc_.
 * @typedef {Object} _mailroom.Verdict `＠record` The SES decision about viruses, spam, _etc_.
 * @prop {string} status Either `PASS`, `FAIL` or `GREY`.
 * @typedef {_mailroom.S3Object} S3Object `＠record` An object in an S3 bucket.
 * @typedef {Object} _mailroom.S3Object `＠record` An object in an S3 bucket.
 * @prop {string} Bucket The name of the bucket.
 * @prop {string} Key A unique key of the object.
 * @prop {string|!Buffer} Body The body of the object.
 */
