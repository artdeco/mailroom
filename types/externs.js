/**
 * @fileoverview
 * @externs
 */


/* typal types/event.xml externs */
/** @const */
var _mailroom = {}
/**
 * An event for Lambda function from SES.
 * @record
 */
_mailroom.event
/**
 * @type {!Array<!_mailroom.Record>}
 */
_mailroom.event.prototype.Records
/**
 * @record
 */
_mailroom.Record
/**
 * @type {!_mailroom.SES}
 */
_mailroom.Record.prototype.ses
/**
 * @record
 */
_mailroom.SES
/**
 * Message metadata.
 * @type {!_mailroom.Mail}
 */
_mailroom.SES.prototype.mail
/**
 * Processing metadata.
 * @type {!_mailroom.Receipt}
 */
_mailroom.SES.prototype.receipt
/**
 * Info about the email message.
 * @record
 */
_mailroom.Mail
/**
 * When the message was received (in ISO8601 format),
 * e.g., `2019-08-05T21:30:02.028Z`.
 * @type {string}
 */
_mailroom.Mail.prototype.timestamp
/**
 * The email address of the message sender,
 * e.g., `ned@ludd.io`.
 * @type {string}
 */
_mailroom.Mail.prototype.source
/**
 * A message ID assigned by Amazon SES,
 * e.g.,`o3vrnil0e2ic28trm7dfhrc2v0clambda4nbp0g1`.
 * @type {string}
 */
_mailroom.Mail.prototype.messageId
/**
 * Who should receive the message, e.g., `[recipient@example.com]`.
 * @type {!Array<string>}
 */
_mailroom.Mail.prototype.destination
/**
 * Whether not all headers were included in the event.
 * @type {boolean}
 */
_mailroom.Mail.prototype.headersTruncated
/**
 * @type {!_mailroom.CommonHeaders}
 */
_mailroom.Mail.prototype.commonHeaders
/**
 * @record
 */
_mailroom.CommonHeaders
/**
 * Where to return the message to.
 * @type {string}
 */
_mailroom.CommonHeaders.prototype.returnPath
/**
 * The `From` header(s).
 * @type {!Array<string>}
 */
_mailroom.CommonHeaders.prototype.from
/**
 * The `Date` header.
 * @type {string}
 */
_mailroom.CommonHeaders.prototype.date
/**
 * The to header.
 * @type {!Array<string>}
 */
_mailroom.CommonHeaders.prototype.to
/**
 * The sender's message id.
 * @type {string}
 */
_mailroom.CommonHeaders.prototype.messageId
/**
 * The `Subject` header.
 * @type {string}
 */
_mailroom.CommonHeaders.prototype.subject
/**
 * @record
 */
_mailroom.Receipt
/**
 * When the message was processes (in ISO8601 format),
 * e.g., `2019-08-05T21:30:02.028Z`.
 * @type {string}
 */
_mailroom.Receipt.prototype.timestamp
/**
 * How long it took to process the message.
 * @type {number}
 */
_mailroom.Receipt.prototype.processingTimeMillis
/**
 * Who received the message.
 * @type {!Array<string>}
 */
_mailroom.Receipt.prototype.recipients
/**
 * @type {!_mailroom.Verdict}
 */
_mailroom.Receipt.prototype.spamVerdict
/**
 * @type {!_mailroom.Verdict}
 */
_mailroom.Receipt.prototype.virusVerdict
/**
 * @type {!_mailroom.Verdict}
 */
_mailroom.Receipt.prototype.spfVerdict
/**
 * @type {!_mailroom.Verdict}
 */
_mailroom.Receipt.prototype.dkimVerdict
/**
 * @type {!_mailroom.Verdict}
 */
_mailroom.Receipt.prototype.dmarcVerdict
/**
 * The SES decision about viruses, spam, _etc_.
 * @record
 */
_mailroom.Verdict
/**
 * Either `PASS`, `FAIL` or `GREY`.
 * @type {string}
 */
_mailroom.Verdict.prototype.status
/**
 * An object in an S3 bucket.
 * @record
 */
_mailroom.S3Object
/**
 * The name of the bucket.
 * @type {string}
 */
_mailroom.S3Object.prototype.Bucket
/**
 * A unique key of the object.
 * @type {string}
 */
_mailroom.S3Object.prototype.Key
/**
 * The body of the object.
 * @type {string|!Buffer}
 */
_mailroom.S3Object.prototype.Body
