#!/usr/bin/env node
'use strict';
const path = require('path');             
const g = path.join;
const l = (c, h, a, f = "-") => {
  var b = [];
  if (!a || Array.isArray(a) && 0 == a.length) {
    throw Error("Empty `from`.");
  }
  f = f.substr(0, 100);
  Array.isArray(a) && (a = a.join(", "));
  a = a.substr(0, 100).replace(/"/g, '\\"');
  if ("string" == typeof c) {
    c = new Date(c);
  } else {
    if (!(c instanceof Date)) {
      throw Error("A string of Date expected.");
    }
  }
  c = (10000000000000 - c.getTime()) / 1000;
  b.forEach(e => {
    if (!e.startsWith("\\")) {
      throw Error(`Attribute ${e} should start with \\`);
    }
  });
  b = b.join("");
  return `${c} ${h}${b.length ? ` ${b}` : ""} "${a}" ${f}`.replace(/[+/]/g, e => encodeURIComponent(e));
};
const m = require("aws-sdk"), {BUCKET:n = "buckets.email"} = process.env, p = new m.S3;
module.exports = {handler:async function(c, h, a) {
  const {ses:{receipt:{recipients:f}, receipt:b, mail:{messageId:e}, mail:q}, ses:r} = c.Records[0];
  let k = "index";
  if ("FAIL" == b.spfVerdict.status || "FAIL" == b.dkimVerdict.status || "FAIL" == b.spamVerdict.status || "FAIL" == b.virusVerdict.status) {
    k = "spam";
  }
  let {timestamp:t, source:u, commonHeaders:{subject:v = "-", from:w = u}} = q;
  const x = l(t, e, w, v);
  await Promise.all(f.map(async d => {
    const [y, z] = d.split("@");
    d = g(z, y);
    d = g(d, k, x);
    d = {Bucket:n, Key:d, Body:JSON.stringify(r)};
    await p.putObject(d).promise();
  }));
  a(null, null);
}};

