const AWS = require(/* depack */ 'aws-sdk')
import { join } from 'path'
import { getIndexKey } from '../lib'

// AWS_NODEJS_CONNECTION_REUSE_ENABLED -> 1

const { 'BUCKET': Bucket = 'buckets.email' } = process.env
// const Bucket = 'buckets.email'
// const domain = 'luddites.co'
// const user = 'ned'

const s3 = new AWS['S3']()

/**
 * Lambda handler for SES events.
 * @param {_mailroom.event} event The event.
 * @param {*} context
 * @param {!Function} callback
 */
export async function handler(event, context, callback) {
  const { ses: { receipt: { recipients }, receipt, mail: { messageId }, mail }, ses } = event.Records[0]
  // console.log("SES Notification:\n", JSON.stringify(sesNotification, null, 2))

  let FOLDER = 'index'

  // Check if any spam check failed
  if (receipt.spfVerdict.status == 'FAIL'
      || receipt.dkimVerdict.status == 'FAIL'
      || receipt.spamVerdict.status == 'FAIL'
      || receipt.virusVerdict.status == 'FAIL') {
    // console.log('Dropping spam')
    FOLDER = 'spam'
    // Stop processing rule set, dropping message
    // return callback(null, {'disposition':'STOP_RULE_SET'});
  }

  let { timestamp, source, commonHeaders: { subject = '-', from = source } } = mail

  await Promise.all(recipients.map(async (d) => {
    const [user, domain] = d.split('@')
    const [actual, ...attrs] = user.split('+')
    const dir = join(domain, actual)

    const meta = getIndexKey(timestamp, messageId, from, subject, attrs.map((a) => `\\${a}`))
    const Key = join(dir, FOLDER, meta)

    /** @type {!_mailroom.S3Object} */
    const index = {
      Bucket,
      Key,
      Body: JSON.stringify(ses),
    }
    await s3['putObject'](index)['promise']()
  }))

  callback(null, null)
}

/**
 * @typedef {import('../../types').event} _mailroom.event
 */
/**
 * @typedef {import('../../types').S3Object} _mailroom.S3Object
 */

// Retrieve the email from your bucket
// const { Body, LastModified } = await s3.getObject({
//     Key,
//     Bucket,
// }).promise()