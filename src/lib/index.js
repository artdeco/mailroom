/**
 * Returns the key used in index.
 * @param {string|!Date} timestamp
 * @param {string} messageId
 * @param {string|!Array<string>} from
 * @param {string} [subject]
 * @param {!Array<string>} [attrs]
 */
export const getIndexKey = (timestamp, messageId, from, subject = '-', attrs = []) => {
  if (!from || (Array.isArray(from) && from.length == 0)) throw new Error('Empty `from`.')

  subject = subject.substr(0, 100)
  Array.isArray(from) && (from = from.join(', '))
  from = from.substr(0, 100).replace(/"/g, '\\"')

  if (typeof timestamp == 'string') timestamp = new Date(timestamp)
  else if (!(timestamp instanceof Date)) throw new Error('A string of Date expected.')

  const D = 10000000000000
  const dd = (D - timestamp.getTime()) / 1000
  attrs.forEach((a) => {
    if (!a.startsWith('\\')) throw new Error(`Attribute ${a} should start with \\`)
  })
  const a = attrs.join('')
  const key = `${dd} ${messageId}${a.length ? ` ${a}` : ''} "${from}" ${subject}`
    .replace(/[+/]/g, (m) => encodeURIComponent(m))
  return key
}