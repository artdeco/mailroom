import env from 'envariable'
import Imap from 'imap'
import { inspect } from 'util'
import { createHash } from 'crypto'
import AWS from 'aws-sdk'
import loading from 'indicatrix'
import { posix } from 'path'
import { c } from 'erte'
import { getIndexKey } from './lib'
import { writeFileSync, readFileSync } from 'fs'
import { confirm } from 'reloquent'
env()
const { join } = posix

const Bucket = 'buckets.email'
const s3 = new AWS.S3()

const { WORKMAIL_USER: user, WORKMAIL_PASSWORD: password } = process.env

const imap = new Imap({
  user,
  password,
  host: 'imap.mail.eu-west-1.awsapps.com',
  port: 993,
  tls: true,
})

const [u, d] = user.split('@')
const Prefix = join(d, u)

console.log('Transferring email from %s@%s', c(u, 'green'), c(d, 'yellow'))

const getMessage = async (id) => {
  const f = imap.seq.fetch(`${id}:${id}`, {
    bodies: '',
    struct: true,
  })
  return await new Promise((r, j) => {
    let buffer = '', attrs, headers

    f.on('message', (msg, seqno) => {
      console.log('Message #%d', seqno)
      var prefix = '(#' + seqno + ') '
      msg.on('body', (stream, info) => {
        stream.on('data', (chunk) => {
          buffer += chunk.toString('utf8')
        })
        stream.once('end', () => {
          headers = Imap.parseHeader(buffer)
          // console.log(prefix + 'Parsed header: %s', inspect())
          // console.log(buffer)
        })
        stream.on('error', j)
      })
      msg.once('attributes', (a) => {
        attrs = a
      })
      msg.once('end', () => {
        // console.log(prefix + 'Finished')
      })
    })
    f.once('error', (err) => {
      j(err)
      // console.log('Fetch error: ' + err)
    })
    f.once('end', function() {
      r({ buffer, attrs, headers })
      // console.log('Done fetching all messages!')
    })
  })
}

const CURSOR = `cursors/${u}@${d}.json`

const transfer = async () => {
  let i = 0
  try {
    i = JSON.parse(readFileSync(CURSOR, 'utf8')) + 1
  } catch (err) {
    i = 1
    /**/
  }
  const { buffer, attrs: { flags = [] }, attrs, headers } = await getMessage(i)
  const id = createHash('sha1').update(buffer).digest('hex')
  // console.log(id + 'Attributes: %s', inspect(attrs, false, 8, true))
  // console.log(headers)
  const { subject = [], from = [] } = headers
  const Key = join(Prefix, id)
  const ignore = ['NameCheap', 'OutSystems', 'Bubble']
  const [S = '-'] = subject
  if (new RegExp(ignore.join('|'), 'i').test(headers.from) || /Successfully published/i.test(S)) {
    writeFileSync(CURSOR, i)
    console.log('Ignoring (%s)\n     %s\nfrom %s', attrs.date.toLocaleString(), c(S, 'yellow'), c(headers.from, 'red'))
    return
  }
  await loading(`Uploading ${c(headers.from || '', 'yellow')}`, s3.putObject({
    Bucket,
    Key,
    Body: buffer,
  }).promise())
  const meta = getIndexKey(attrs.date, id, from, S, flags)

  const gch = getCommonHeader.bind(null, headers)
  /**
   * @type {import('../types').SES}
   */
  const ses = {
    mail: {
      destination: user,
      source: gch('from'),
      timestamp: attrs.date,
      messageId: id,
      commonHeaders: {
        date: gch('date'),
        from: gch('from', false),
        messageId: gch('message-id'),
        returnPath: gch('return-path'),
        subject: gch('subject'),
        to: gch('to', false),
      },
    },
  }
  const Index = join(Prefix, 'index', meta)
  const put = s3.putObject({
    Bucket,
    Key,
    Body: buffer,
  }).promise()
  const index = s3.putObject({
    Bucket,
    Key: Index,
    Body: JSON.stringify(ses),
  }).promise()

  const res = await loading(`${i}) Uploading ${c(headers.from || '', 'yellow')}`, Promise.all([put, index]))
  console.log(Index)

  // console.log(res)
  // console.log(meta)
  writeFileSync(CURSOR, i)
}

let cont = true
imap.once('ready', () => {
  // imap.getBoxes((err, boxes) => {
  //   console.log(boxes)
  // })

  const loop = async () => {
    await transfer()
    cont = true
    // const cont = await confirm('continue')
    if (cont) return loop()
  }
  imap.openBox('INBOX', true, async (err, box) => {
    if (err) throw err
    await loop()
    // console.log(res)
    imap.end()
  })
})

const getCommonHeader = (headers, name, zero = true) => {
  try {
    if (zero)
      return headers[name][0]
    return headers[name]
  } catch (err) {
    return undefined
  }
}

imap.once('error', (err) => {
  console.log(err)
  cont = false
})

imap.once('end', function() {
  console.log('Connection ended')
  cont = false
})

imap.connect()