
  // If DMARC verdict is FAIL and the sending domain's policy is REJECT
  // (p=reject), bounce the email.
  if (receipt.dmarcVerdict.status === 'FAIL' && receipt.dmarcPolicy.status === 'REJECT') {
      // The values that make up the body of the bounce message.
      const sendBounceParams = {
          BounceSender: `mailer-daemon@${emailDomain}`,
          OriginalMessageId: messageId,
          MessageDsn: {
              ReportingMta: `dns; ${emailDomain}`,
              ArrivalDate: new Date(),
              ExtensionFields: [],
          },
          // Include custom text explaining why the email was bounced.
          Explanation: "Unauthenticated email is not accepted due to the sending domain's DMARC policy.",
          BouncedRecipientInfoList: receipt.recipients.map((recipient) => ({
              Recipient: recipient,
              // Bounce with 550 5.6.1 Message content rejected
              BounceType: 'ContentRejected',
          })),
      }

      // console.log('Bouncing message with parameters:');
      // console.log(JSON.stringify(sendBounceParams, null, 2));
      // Try to send the bounce.
      new AWS.SES().sendBounce(sendBounceParams, (err, data) => {
          // If something goes wrong, log the issue.
          if (err) {
              console.log(`An error occurred while sending bounce for message: ${messageId}`, err);
              callback(err);
          // Otherwise, log the message ID for the bounce email.
          } else {
              console.log(`Bounce for message ${messageId} sent, bounce message ID: ${data.MessageId}`);
              // Stop processing additional receipt rules in the rule set.
              callback(null, {
                  disposition: 'stop_rule_set',
              });
          }
      });
  }
  // If the DMARC verdict is anything else (PASS, QUARANTINE or GRAY), accept
  // the message and process remaining receipt rules in the rule set.
