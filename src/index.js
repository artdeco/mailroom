import envariable from 'envariable'
import idio, { Router } from '@idio/idio'
import { S3 } from 'aws-sdk'
import { join } from 'path'
import loading from 'indicatrix'
import initRoutes, { watchRoutes } from '@idio/router'
import { c } from 'erte'
import bootstrap from '@a-la/bootstrap/bootstrap/bootstrap.json'
import renameMap from '@a-la/bootstrap/bootstrap/camel.json'

envariable()

const Bucket = 'buckets.email'
const s3 = new S3()
// const Prefix = 'luddites.co/ned/'
const folder = 'adc.sh'
const user = 'hello'
const Prefix = join(folder, user, '/')
const Db = join(folder, '_db')

;(async () => {
  const { url, app, router, middleware } = await idio({
    static: {
      use: true,
      root: 'static',
    },
    frontend: {
      use: true,
      override: {
        // preact: '/node_modules/preact/src/preact.js',
      },
      jsxOptions: {
        prop2class: true,
        classNames: bootstrap,
        renameMap: renameMap,
      },
      hotReload: true,
    },
    form: {
      none: true,
    },
    jsonErrors: true,
  })
  Object.assign(app.context, {
    Bucket,
    Prefix,
    Db,
    s3,
  })
  const api = new Router()
  const w = await initRoutes(api, 'api', { middleware })
  watchRoutes(w)
  router.use('/api', middleware.jsonErrors, api.routes())
  app.use(router.routes())

  // await upd(1)
  console.log(url)
})()

const D = 10000000000000

const upd = async (MaxKeys) => {
  const objects = await s3.listObjectsV2({
    Bucket,
    MaxKeys,
    Prefix,
  }).promise()
  console.log('Total: %s', objects.Contents.length)
  const { Contents } = objects
  await Contents.reduce(async (acc, { Key, LastModified }) => {
    await acc
    if (/^me\/\d+$/.test(Key)) return acc
    const { Body } = await loading(`Loading ${c(Key, 'yellow')}`, s3.getObject({
      Bucket,
      Key,
    }).promise())

    const dd = (D - LastModified.getTime()) / 1000
    const ToAdd = `${Prefix}/${dd}`
    await s3.putObject({
      Bucket,
      Key: ToAdd,
      Body,
    }).promise()
    console.log('Added key %s with length', c(ToAdd, 'green'), Body.length)
    // await s3.deleteObject({
    //   Bucket,
    //   Key,
    // }).promise()
    console.log('Removed key %s', c(Key, 'red'))
  }, {})
}