// https://github.com/mathiasbynens/quoted-printable/blob/master/src/quoted-printable.js
export const decode = function(input, unicode = false) {
  const i = input
    .replace(/[\t\x20]$/gm, '')
    .replace(/=(?:\r\n?|\n|$)/g, '')
  if (unicode) return i.replace(/(=(?:[a-fA-F0-9]{2}))+/g, (Q) => {
    return utf8QuotedPrintableDecode(Q)
  })
  return i.replace(/=([a-fA-F0-9]{2})/g, function($0, $1) {
    var codePoint = parseInt($1, 16)
    return String.fromCharCode(codePoint)
  })
}

export const replaceAssets = (html, inlines = {}) => {
  const replaced = []
  const h = html
    // .replace(/url\s*\(\s*(['"])([\1]*?)\1\)/g, (m,mm,url) => {
    //   replaced.push(url)
    //   return ''
    // })
    .replace(/url\s*\(([\s\S]+?)\)/g, (m,url) => {
      // console.log(url)
      url = url.trim()
      try {
        url = decodeURIComponent(url)
      } catch (err) {/**/}
      replaced.push(url)
      return ''
    })
    .replace(/[\s'"](?:src|srcset|background)?\s*=\s*(['"])([^\1]*?)\1/g, (m,mm,src) => {
      if (!src) return m // empty
      if (`<${src.replace('cid:', '')}>` in inlines) {
        // debugger
        return m
      }
      replaced.push(src)
      return ''
    })
    .replace(/<\s*link[^>]*?([\s'"]href\s*=\s*(['"])([^\2]*?)\2)/g, (m,attr,q,src) => {
      if (!src) return m// empty
      replaced.push(src)
      return m.replace(attr, '')
    })
  return { h, replaced }
}

/**
 * Decode a UTF-8 quoted printable string into a UTF8 string
 * https://gist.github.com/t3hmrman/72ae227e4d84406fe89d
 *
 * @param {string} str - UTF quoted printable string (ex. "=E8=97=A4=E6=A3=AE")
 */
function utf8QuotedPrintableDecode(str) {
  // Grab the ascii bytes separated by "=", trim first/last elements if empty
  var asciiHex = str.split(/=+/)
  if (asciiHex[0] == '') { asciiHex.shift() }
  if (asciiHex[asciiHex.length - 1] == '') { asciiHex.pop() }

  // Convert ascii hex into bytes
  var bytes = []
  for (var i = 0; i < asciiHex.length; i++) {
    bytes.push(String.fromCharCode(parseInt(asciiHex[i], 16)))
  }

  // Return UTF8 decoded string (trick outlined @ http://ecmanaut.blogspot.co.uk/2006/07/encoding-decoding-utf8-in-javascript.html)
  const e = escape(bytes.join(''))
  const utf8String = decodeURIComponent(e)
  return utf8String
}