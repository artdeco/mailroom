import Loadable from '../Loadable'
import '@a-la/bootstrap/preact'
import './index.css'
import { Spinner, SpinnerSm, Link, MoveButton } from '../lib'
import { replaceAssets, decode } from './lib'

class Message extends Loadable {
  constructor() {
    super()
    this.state = {
      // body: null,
    }
    this.mid = null
  }
  componentDidMount() {
    // this.load()
    this.componentWillReceiveProps(this.props)
  }
  /**
   * The component is guaranteed to receive message in props.
   * @param {*} props
   */
  componentWillReceiveProps(props) {
    if (this.mid == props.message.MessageId) return
    this.load(props.message)
  }
  async _load(msg) {
    const { MessageId } = msg
    if (!MessageId) throw new Error('MessageId is not passed to the _load method.')
    const { message } = await this.fetch(`/api/message?MessageId=${encodeURIComponent(MessageId)}`)
    if (!message) return

    const { headers, parsedHeaders = {}, from, to, date, subject,
      encoding, charset, alts, parts } = message
    let { html, plain } = message
    let replaced = []

    const Comp = getComp({ html, plain, alts, encoding, charset, replaced })

    this.setState({
      headers, parsedHeaders, html, plain, from, to, date: new Date(date),
      subject, replaced, Comp,
    })
    this.mid = MessageId
    if (this.props.onLoad) this.props.onLoad(msg)
  }
  render({ className, style, message, onDelete }) {
    const { loading, error } = this.state

    // if (error) return ()
    // if (loading) return (<div>Loading...</div>)
    // if (!body) return null

    const { subject = message.Subject, headers: rawHeaders,
      to, from  = message.From, date = message.Received,
      replaced = [], Comp } = this.state

    return (<div mb-3 className={className} style={style}>
      {subject && <h2>
        <em>{subject}</em>
        {loading && <Spinner ml-1 style="font-size:1rem;" />}
      </h2>}
      {(from || to || date) && <dl row>
        {from && <dt colSm3>From</dt>}
        {from && <dd colSm9>{from}</dd>}
        {to && <dt colSm3>To</dt>}
        {to && <dd colSm9>{to}</dd>}
        {date && <dt colSm3>Date</dt>}
        {date && <dd colSm9>{date.toLocaleString()}</dd>}
      </dl>}
      {rawHeaders && <details mb3>
        <summary textRight textSecondary><em>Show Headers</em></summary>
        <pre Headers>{rawHeaders}</pre>
      </details>}
      {Boolean(replaced.length) && <details mb3>
        <summary textRight textSecondary><em>External Images</em></summary>
        <ul Headers textMonospace>
          {replaced.map((img) => <li textBreak key={img}>{img}</li>)}
        </ul>
      </details>}

      {error && <div textWhite bgDanger>Error: {error}</div>}
      {<Comp />}
      <hr />
      <MoveButton btn btnDanger onComplete={onDelete} message={message}>Delete</MoveButton>
      {' '}
      <MoveButton btn btnWarning onComplete={onDelete} section="spam" message={message}>Spam</MoveButton>
    </div>)
  }
}

const getComp = ({ html, plain, alts, encoding, charset, replaced = [], inlines }) => {
  if (plain) {
    return function PlainComp() {
      if (encoding == 'quoted-printable') {
        const unicode = /utf-8/i.test(charset)
        plain = decode(plain, unicode)
      }
      return (<pre w100 pl1 style="border-left:2px solid lightcoral;">
        {plain}
      </pre>) }
  }
  if (html) {
    const hh = procHTML(html, encoding, charset)
    return function IFrameComp() { return (<IFrame replaced={replaced} html={hh} inlines={inlines} />) }
  }

  const htmlAlt = alts.find(({ type }) => {
    return type == 'text/html'
  })
  if (htmlAlt) {
    const { html: ht, encoding: e, charset: c } = htmlAlt
    const hh = procHTML(ht, e, c)
    // replaced.push(...r)
    return function IFrameComp() { return (<IFrame replaced={replaced} html={hh} inlines={inlines} />) }
  }
  const plainAlt = alts.find(({ type }) => {
    return type == 'text/plain'
  })
  if (plainAlt) {
    const { plain: p } = plainAlt
    return function PlainComp() { return (<pre w100 pl1 style="border-left:2px solid lightcoral;">
      {p}
    </pre>) }
  }
  const relatedAlt = alts.find(({ type }) => {
    return type == 'multipart/related'
  })
  if (relatedAlt) {
    return () => (<Related parts={relatedAlt.parts} replaced={replaced} />)
  }
  return null
}

const Related = ({ parts, replaced }) => {
  const mainParts = parts.filter(({ type }) => {
    return ['text/html', 'text/plain'].includes(type)
  })
  const inlines = parts.reduce((acc, part) => {
    const { 'Content-Disposition': cd, 'Content-Id': ci } = part.parsedHeaders
    if (!/inline/.test(cd)) return acc
    acc[ci] = part
    return acc
  }, {})
  const [mainPart] = mainParts
  if (!mainPart) return null
  const Comp = getComp({ ...mainPart, replaced, inlines })
  return (<Comp />)
}

const procHTML = (html, encoding, charset) => {
  const unicode = /utf-8/i.test(charset)
  if (encoding == 'base64') {
    html = atob(html)
    if (unicode) {
      html = decodeURIComponent(html.split('').map((c) => {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
      }).join(''))
    }
  } else if (encoding == 'quoted-printable') {
    html = decode(html, unicode)
  }
  return html
}

/**
 * Render HTML in a sandboxed iframe (without JS execution).
 */
const IFrame = ({ html, inlines = {}, replaced = [] }) => {
  if (!html) return null
  const { h: hh, replaced: r } = replaceAssets(html, inlines)
  replaced.push(...r)

  let srcDoc = getHtml(hh, inlines)
  Object.entries(inlines).forEach(([cid, part]) => {
    const { name, encoding, data, type } = part
    let vv
    if (type.startsWith('image')) {
      vv = `data:${type};${encoding},${data}`
      // vv = `<img src="${vv}" title="${name}" alt="${name}">`
    }
    if (vv) srcDoc = srcDoc.replace(`cid:${cid.replace(/^<(.+?)>$/, '$1')}`, vv)
  })
  return (<iframe onLoad={({ target }) => {
    const h = target.contentWindow.document.body.scrollHeight
    target.style.height = `${h}px`
  }} HTMLIFrame w100 pl1 sandbox="allow-same-origin" srcDoc={srcDoc}
  style="border-left:2px solid lightcoral" />)
}

const iframeCSS = `
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">-->
<style>
  body {
    word-break: break-word;
  }
</style>`
const getHtml = (html) => {
  let i = iframeCSS
  if (/<meta[\s\S]*?\sname\s*=\s*(['"])viewport\1/i.test(html)) i = i
    .replace('<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">\n', '')
  if (/<meta[\s\S]*?\scharset\s*=\s*(['"])utf-8\1/i.test(html)) i = i
    .replace('<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">\n', '')
  if (/<head>/.test(html)) return html.replace('<head>', `<head>${i}`)
  return `${i}${html}`
}

export default Message