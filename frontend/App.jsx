import { Component } from 'preact'
import './index.css'
import Message from './Message'
import List from './List'
import '@a-la/bootstrap/preact'
import { Icon } from './lib'
import Loadable from './Loadable'

export default class App extends Component {
  constructor() {
    super()
    this.prefix = 'luddites.co/ned'
    this.state = {
      selected: null,
      section: 'index',
    }
    this.list = null
    this.selectSection = this.selectSection.bind(this)
    this.seen = null
    this.refList = (list) => this.list = list
    this.refMessage = (message) => this.message = message
    this.select = this.select.bind(this)
  }
  // componentDidMount() {
  //   const listener = () => {
  //     if (this.iframe) {
  //       this.iframe.width =
  //         this.iframe.parentElement.getBoundingClientRect().width - 15
  //     }
  //   }
  //   window.addEventListener('resize', listener)
  //   this.listener = listener
  // }
  // componentWillUnmount() {
  //   window.removeEventListener(this.listener)
  // }
  markRead(MessageId) {
    const current = this.getMessage(MessageId)
    current.read = new Date()
    this.setMessage(MessageId, current)
  }
  getKey(MessageId) {
    return `${this.prefix}/${MessageId}`
  }
  setMessage(MessageId, value) {
    const v = JSON.stringify(value)
    localStorage.setItem(this.getKey(MessageId), v)
  }
  getMessage(MessageId) {
    let msg = localStorage.getItem(this.getKey(MessageId)) || '{}'
    msg = JSON.parse(msg)
    if (msg.read) msg.read = new Date(msg.read)
    return msg
  }
  select(selected) {
    this.setState({
      selected,
    })
  }
  selectSection(section) {
    if (this.state.section == section) return
    // this.unselect()
    this.setState({
      selected: null,
      section,
    })
  }
  render() {
    const { selected, section } = this.state

    return (<div container>
      <div row>
        <div colSm9>
          {!selected && <h2>
            <em>Please select a message.</em>
          </h2>}
          {selected && <Message ref={this.refMessage} message={selected} onLoad={async (message) => {
            if (!this.seen) return
            const { MessageId, Key } = message

            const res = await this.seen.see(Key)
            // todo: make list read attributes
            if (res) this.markRead(MessageId)
            if (typeof res == 'string') {
              // update the key of the message in the list
              message.Key = res
              message.Attributes.unshift('Seen')
              if (this.list) this.list.forceUpdate()
              if (this.message) this.message.forceUpdate()
              // force update message too for buttons
            }
          }} onDelete={(message) => {
            this.list.removeItem(message)
            this.select(null)
          }} id="Message" />}
          <Seen ref={(seen) => {
            this.seen = seen
          }} />
        </div>

        <div colSm3>
          <h2>Messages</h2>
          <div style="margin-left: .2rem; padding-right: .4rem" btnBlock btnGroup btnGroupSm role="group" aria-label="Basic example">
            <SectionBtn onClick={this.selectSection} section={section} target="index" icon="Inbox" text="Inbox" />
            <SectionBtn onClick={this.selectSection} section={section} target="trash" icon="Trash" text="Deleted" />
            <SectionBtn onClick={this.selectSection} section={section} target="spam" icon="Exclamation-Diamond" text="Spam" />
          </div>
          <List onDelete={(message) => {
            this.list.removeItem(message)
            if (this.state.selected == message)
              this.select(null)
          }} section={section} ref={this.refList} selected={selected} onSelect={this.select}/>
        </div>
      </div>
    </div>)
  }
}

class Seen extends Loadable {
  /**
   * Update attributes of the index key to have \Seen flag.
   * @param {string} key The key of the index.
   */
  async see(key) {
    return await this.load(key)
  }
  async load(key) {
    const body = new FormData()
    body.append('Key', key)
    const { seen } = await this.fetch('/api/seen', {
      method: 'POST',
      body,
    })
    this.setState({
      seen,
    })
    return seen
  }
  render() {
    return null
  }
}

const SectionBtn = ({ section, target, icon, text, onClick }) => {
  return (<button onClick={() => onClick(target)} btn className={(section == target ? 'btn-outline-dark btn-light' : 'btn-secondary')} type="button">
    <Icon icon={icon} /> {text}
  </button>)
}

/**
 * @suppress {nonStandardJsDocs}
 * @typedef {import('../types').Message} _mailroom.Message
 */