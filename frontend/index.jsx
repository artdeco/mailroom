import { render } from 'preact'
import App from './App'
import './index.css'

import hot from '@idio/hot-reload'

let app = render(<App />, window['app-container'])

hot(() => {
  app = render(<App />, window['app-container'], app)
})