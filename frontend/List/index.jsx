import Loadable from '../Loadable'
import '@a-la/bootstrap/preact'
import './index.css' //?prefix=.MessageList
import './item.css' //?prefix=.MessageItem
import { SpinnerSm, Link, MoveButton, Icon } from '../lib'

const mapDate = (m) => {
  m.Received = new Date(m.Date)
  return m
}

export default class List extends Loadable {
  constructor() {
    super()
    this.state = {
      /** @type {!Array<!_mailroom.Message>} */
      messages: [],
      hasMore: true,
      checked: [],
    }
    this.section = null
  }
  removeItem(msg) {
    const { messages } = this.state
    const m = messages.filter((M) => {
      return M != msg
    })
    if (messages.length == m.length) {
      console.error('Message with index key "%s" wasn\'t found.', msg.Key)
      return
    }
    const state = this.uncheck(msg, true)
    this.setState({
      ...state,
      messages: m,
    })
  }
  componentDidMount() {
    this.componentWillReceiveProps(this.props)
    // this.load()
  }
  componentWillReceiveProps(props) {
    // this.load()
    if (this.section == props.section) return
    this.load(null, props.section)
  }
  async _load(after, section) {
    const { messages: current, contoken } = this.state
    // const sectionChanged = this.section == this.props.section

    const form = new FormData()
    if (after === true && contoken) {
      form.append('ContinuationToken', contoken)
    }
    if (section) form.append('Section', section)

    const { messages, next } = await this.fetch('/api/messages', {
      body: form,
      method: 'POST',
    })
    if (!messages) return
    const hasMore = !!next


    if (section) {
      this.section = section
      const mm = messages.map(mapDate)
      return this.setState({
        messages: mm,
        hasMore,
        contoken: next,
      })
    }

    const mm = messages
      .map(mapDate)
      .filter(({ Key }) => {
        if (current.some(({ Key: k }) => k == Key)) return false
        return true
      })

    const msgs = (after ? [...current, ...mm] : [...mm, ...current])
      .sort(({ Received: a }, { Received: b }) => {
        if (a < b) return 1
        if (b > a) return -1
        return 0
      })

    this.setState({
      messages: msgs,
      hasMore,
      contoken: next,
    })
  }
  // /**
  //  * This is called when a flag is added to an index key.
  //  * @param {string} Key
  //  * @param {string} NewKey
  //  */
  // updateKey(Key, NewKey) {
  //   const message = this.state.messages.find((m) => {
  //     return m.MessageId == Key
  //   })
  //   if (!message) {
  //     console.warn('A message with MessageId %s is not found.', Key)
  //     return
  //   }
  //   message.Key = NewKey
  //   this.setState({
  //     messages: [...this.state.messages],
  //   })
  // }
  render({ onSelect, selected, onDelete }) {
    const { error, loading, messages, hasMore, checked } = this.state
    const hasChecked = !!checked.length
    return (<ul MessageList>
      {error && <div textWhite bgDanger>
        Error: {error}
      </div>}

      <div my-1 style="margin-left:.2rem; padding-right:.4rem;" btnBlock btnGroup btnGroupSm>
        <Link btnSuccess btn btnSm role="button" onClick={() => {
          if (!loading) this.load(false)}}
        ><Icon icon="arrow-clockwise" /> {!hasChecked ? 'Update' : ''} {loading && <SpinnerSm />}</Link>
        {hasChecked &&
        <div btn disabled btnOutlineDark>
          {checked.length} item{checked.length > 1 ? 's' : ''}
        </div>}
        {hasChecked &&
        <MoveButton btn btnOutlineDanger
          onComplete={onDelete} messages={checked}>Remove</MoveButton>
        }
        {hasChecked &&
        <MoveButton btn btnOutlineWarning
          onComplete={onDelete} messages={checked}>Junk</MoveButton>
        }
      </div>

      {messages.map((message) => {
        const { MessageId } = message
        return (<ListItem checked={checked.includes(message)} message={message} selected={selected}
          onSelect={onSelect} key={MessageId}
          onCheck={(msg, ch) => {
            if (!ch) return this.uncheck(msg)
            this.setState({
              checked: [...checked, msg],
            })
          }}
        />)
      })}
      {hasMore && <li>
        <Link $loading-disabled $loading-textMuted style={loading ? 'color: grey' : ''} onClick={() => {
          if (!loading) this.load(true)
        }}>Load more {loading && <SpinnerSm />}</Link>
      </li>}
    </ul>)
  }
  uncheck(message, returnState) {
    const { checked } = this.state
    const newChecked = checked.filter(({ Key }) => {
      return Key != message.Key
    })
    if (newChecked.length == checked.length) {
      return returnState ? {} : undefined
    }
    const state = {
      checked: newChecked,
    }
    if (returnState) return state
    this.setState(state)
  }
}

class ListItem extends Loadable {
  // constructor() {
  //   super()
  //   this.state = {
  //     checked: false,
  //   }
  // }
  // get checked() {
  //   return this.state.checked
  // }
  // set checked(checked) {
  //   this.setState({
  //     checked,
  //   })
  // }
  render({ message, selected, onSelect, onCheck, checked }) {
    // const { checked: isChecked } = this.state

    const { MessageId, Subject, From, FromName, FromEmail, Received, Attributes = [] } = message
    const isRead = Attributes.includes('Seen')
    const isSelected = selected && selected.MessageId == MessageId

    return (<li $isSelected-MessageSelected $read-MessageRead className={[
      isSelected ? 'MessageSelected' : '',
      isRead ? 'MessageRead' : '',
      checked ? 'MessageChecked' : '',
    ].join(' ')}>
      <a href="#" onClick={(e) => {
        e.preventDefault()
        if (isSelected) onSelect(null)
        else onSelect(message)
        return false
      }}>
        <span dBlock ListHeader>
          <span dBlock Subject>
            {!isRead && <span dInline badge badgePill className={(checked ? 'badge-info' : 'badge-success')}>new</span>}{' '}
            <span SubjectText>{Subject}</span>
          </span>
          <span pl1 From>
            {FromName && FromEmail && <abbr title={FromEmail}>{FromName}</abbr>}
            {!(FromName && FromEmail) && From}</span>
        </span>
        <span ListDate textMonospace small mt1 py1>
          on {Received.toLocaleDateString()}
          {' '}at {Received.toLocaleTimeString()}
          {' '}<input type="checkbox" checked={checked} onClick={(ev) => {
            // ev.preventDefault()
            ev.stopPropagation()
            ev.stopImmediatePropagation()
            // this.checked = !this.checked
            if (onCheck) onCheck(message, !checked)
            return false
          }}/>
        </span>
      </a></li>)
  }
}