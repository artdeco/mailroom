import { Component } from 'preact'

/**
 * A component that can fetch data from the server.
 * Controls the `loading` and `error` properties of the state.
 */
export default class Loadable extends Component {
  constructor() {
    super()
    this._state = {
      loading: false,
      error: null,
    }
  }
  /**
   * Used in constructors.
   */
  set state(state) {
    this._state = {
      ...this._state,
      ...state,
    }
  }
  get state() {
    return this._state
  }
  set loading(loading) {
    this.setState({
      loading,
    })
  }
  set error(error) {
    this.setState({
      error,
    })
  }
  get error() {
    return this.state.error
  }
  get loading() {
    return this.state.loading
  }
  /**
   * Initialise loading by setting `loading` in state to **true**,
   * clearing current `error` and updating the error in the state
   * if the `_load` method threw.
   * @param  {...any} args Any arguments to be passed to _load.
   */
  async load(...args) {
    try {
      this.loading = true
      this.error = null
      return await this._load(...args)
    } catch (err) {
      this.error = err.message
    } finally {
      this.loading = false
    }
  }
  /**
   * Implement this method in a component that extends the `Loadable` class.
   */
  async _load() {
    throw new Error('The `_load` method has not been implemented.')
  }
  /**
   * Fetch data from the server, parsing JSON. If the `error` property was
   * found in the response, sets the error on the state.
   * @param {!RequestInfo} input
   * @param {!RequestInit} [init]
   */
  async fetch(input, init) {
    const f = await fetch(input, init)
    const { error, ...data } = await f.json()
    if (error) this.error = error
    return data
  }
}