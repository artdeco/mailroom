import Loadable from '../Loadable'

export const SpinnerSm = () => {
  return (<div spinnerBorderSm spinnerBorder role="status">
    <span srOnly>Loading...</span>
  </div>)
}
export const Spinner = ({ className, style }) => {
  return (<div className={className} spinnerBorder style={style} role="status">
    <span srOnly>Loading...</span>
  </div>)
}

export const Icon = ({ icon, title = icon }) => {
  const path = `/icons/${icon}.svg`
  return (<img Icon src={path} alt="" title={title}/>)
}

export const Link = ({ onClick, children, ...props }) => {
  return <a {...props} href="#" onClick={(ev) => {
    ev.preventDefault()
    onClick()
    return false
  }}>{children}</a>
}

export class MoveButton extends Loadable {
  async _load(messages, section, onComplete) {
    // if (!index) throw new Error('IndexId is not passed.')
    if (!Array.isArray(messages)) throw new Error('Expected to see messages to move to trash.')

    await Promise.all(messages.map(async (message) => {
      const body = new FormData()
      body.append('IndexId', message.Key)
      if (section) body.append('Section', section)
      const { status } = await this.fetch('/api/message', {
        method: 'DELETE',
        body,
      })
      if (status) console.log('Moved to %s', status)
      if (status) onComplete(message)
    }))
  }
  render({ onComplete, message, messages = [message], section, children, className, ...rest }) {
    const { error, loading } = this.state
    return (<Link role="button" $loading-disabled className={className} {...rest} onClick={() => {
      this.load(messages, section, onComplete)
    }}>{children} {loading && <SpinnerSm />}</Link>)
  }
}