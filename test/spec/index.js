import { equal } from '@zoroaster/assert'
import { parseFrom } from '../../api/post/messages'

// const Context = require('../context')
// const mailroom = require('../..')

// export default {
//     context,
//     'should be a function'() {
//         equal(typeof mailroom, 'function')
//     },
//     'should call package without error'() {
//         assert.doesNotThrow(() => {
//             mailroom()
//         })
//     },
// }

// module.exports = mailroomTestSuite
//

export const ParseFrom = {
  'parses standard from'() {
    const from = 'janedoe@mailroom.com'
    const res = parseFrom(`"${from}"`)
    equal(res, from)
  },
  'parses standard from with escaped \\"'() {
    const from = '\\"jane-doe\\"'
    const res = parseFrom(`"${from}"`)
    equal(res, from)
  },
  'parses with escaped \\"'() {
    const from = '\\"npm, Inc.\\" <npm@npmjs.com>'
    const s = `"${from}" Enhanced security, user management & SSO now available`
    const res = parseFrom(s)
    equal(res, from)
  },
  'parses with " in text'() {
    const from = 'janedoe@mailroom.com'
    const s = `"${from}" Enhanced "security", user management & SSO now available`
    const res = parseFrom(s)
    equal(res, from)
  },
}