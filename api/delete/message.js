import { posix } from 'path'
const { join } = posix

/**
 * @type {import('../../types').Middleware}
 */
export default async function message(ctx) {
  const { IndexId: Key, Section = 'trash' } = ctx.request.body
  if (!Key) throw new Error('IndexId is expected.')

  const Moved = Key.replace(join(ctx.Prefix, 'index'), join(ctx.Prefix, Section))

  const { Body } = await ctx.s3.getObject({
    Key,
    Bucket: ctx.Bucket,
  }).promise()

  const put = ctx.s3.putObject({
    Bucket: ctx.Bucket,
    Body,
    Key: Moved,
  }).promise()
  const del = ctx.s3.deleteObject({
    Bucket: ctx.Bucket,
    Key,
  }).promise()
  await Promise.all([put, del])

  ctx.body = { status: Section }
}

export const middleware = ['form']