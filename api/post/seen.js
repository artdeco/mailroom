import { posix } from 'path'
import { simpleParseKey } from './messages'
const { join } = posix

export const parseFrom = (title) => {
  const [, from = ''] = /"(.+(?!\\"))"/.exec(title) || []
  return from
}

/**
 * @type {import('../../types').Middleware}
 */
export default async function $messages(ctx) {
  const { Key } = ctx.request.body
  if (!Key) throw new Error('Key is expected.')
  const { key, id, Title, attrs } = simpleParseKey(Key)
  // console.log(key, id, Title, attrs)
  if (attrs.includes('Seen')) return ctx.body = { seen: true }
  else attrs.unshift('Seen')
  const Attrs = attrs.map(a => `\\${a}`)
  const Seen = `${key} ${id} ${Attrs.join('')} ${Title}`
  console.log('See: %s', Title)

  const { Body } = await ctx.s3.getObject({
    Key,
    Bucket: ctx.Bucket,
  }).promise()

  const put = ctx.s3.putObject({
    Bucket: ctx.Bucket,
    Body,
    Key: Seen,
  }).promise()
  const del = ctx.s3.deleteObject({
    Bucket: ctx.Bucket,
    Key,
  }).promise()
  await Promise.all([put, del])

  ctx.body = { seen: Seen }
}

export const middleware = ['form']