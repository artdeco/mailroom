import { posix } from 'path'
const { join } = posix

/**
 *
 * @param {string} title
 */
export const parseFrom = (title) => {
  if (!title.startsWith('"')) throw new Error('Expected opening " for FROM')
  let j
  title.replace(/[^\\]"/, (m, i) => {
    j = i
  })
  if (!j) throw new Error('Expected closing " for FROM')
  const from = title.substr(0, j + 1).replace('"', '')
  return from
  // const [, from = ''] = /"((?!\\")(.+))"/.exec(title) || []
  // return from
}

const cache = false

/**
 * @type {import('../../types').Middleware}
 */
export default async function $messages(ctx) {
  const { MaxKeys = 10, ContinuationToken, Section = 'index' } = ctx.request.body
  // console.log(ctx.Bucket, ctx.Prefix)
  const Prefix = join(ctx.Prefix, Section)
  // console.log(Prefix)
  const G = `MESSAGES_${Section}`
  const N = `NextContinuationToken${Section}`

  if (!cache || !global[G]) {
    const res = await ctx.s3.listObjectsV2({
      Bucket: ctx.Bucket,
      Prefix,
      ContinuationToken,
      MaxKeys,
    }).promise()
    const messages = res.Contents
      .map((m) => {
        return parseKey(m, Prefix)
      })
    global[G] = messages
    global[N] = res.NextContinuationToken
  }
  const messages = global[G]
  const next = global[N]

  ctx.body = { messages, next }
}

export const simpleParseKey = (Key) => {
  let [key, id, ...rest] = Key.split(' ')
  /** @type {string[]} */
  const attrs = []
  if (!rest[0].startsWith('"')) { // attributes
    attrs.push(...rest[0].split('\\').filter(Boolean).map(f => f.trim()))
    rest = rest.slice(1)
  }
  const Title = rest.join(' ')
  return { Title, attrs, id, key }
}

export const parseKey = (m, Prefix) => {
  const { Key } = m
  const { id, key, Title, attrs } = simpleParseKey(Key)

  const from = parseFrom(Title)
  // console.log(from)
  const subject = Title.substr(from.length + 2 + 1) // 2 " and 1 space
    .replace(/%2(b|f)/ig, (s) => decodeURIComponent(s))
  m.Title = `${subject} ${from}`
  m.Subject = subject
  m.From = from.replace(/\\"/g, '')
  m.Attributes = attrs
  const [,fromName, fromEmail] = /(.+?)<(.+?)>$/.exec(from) || ['', '', '']
  // console.log(fromName)
  m.FromName = fromName.trim().replace(/^\\?"(.+?)\\?"$/, '$1')
  m.FromEmail = fromEmail.trim()
  m.MessageId = id
  const d = key.replace(`${Prefix}/`, '').trim()

  m.Date = new Date(10000000000000 - d * 1000)

  delete m.ETag
  delete m.StorageClass
  return m
}

export const middleware = ['form']