import { posix } from 'path'
import { decode } from '../../frontend/Message/lib'
const { join } = posix
global.CACHED = global.CACHED || {}

/**
 * @type {import('../../types').Middleware}
 */
export default async function message(ctx) {
  const { MessageId } = ctx.query
  if (!MessageId) throw new Error('MessageId is expected.')

  // const prefix = ctx.Db
  const prefix = ctx.Prefix
  const Key = join(prefix, MessageId)

  if (!(MessageId in global.CACHED)) {
    const res = await ctx.s3.getObject({
      Bucket: ctx.Bucket,
      Key,
    }).promise()
    const s = res.Body.toString()
    global.CACHED[MessageId] = s
  }
  const s = global.CACHED[MessageId]

  const { parsedHeaders, headers, ...rest } = parseBody(s)

  // let { headers, body } = splitBody(s)
  // const lines = getHeaderLines(headers)
  // const parsedHeaders = getHeaders(lines)

  const from = findHeader(parsedHeaders, 'from')
  const to = findHeader(parsedHeaders, 'to')
  const date = findHeader(parsedHeaders, 'date')
  const subject = findHeader(parsedHeaders, 'subject')

  ctx.body = { message: { subject, from, to, date, ...rest, headers, parsedHeaders } }
}

/**
 * Returns content type and attributes.
 * @param {string} ct The content-type.
 */
const getTypeAndProps = (ct) => {
  let [type, ...props] = ct.split(/\s*;\s*/)
  props = props.reduce((acc, p) => {
    let [k, ...v] = p.split('=')
    v = v.join('=')
    if (k && v) acc[k] = v.replace(/^['"](.+)['"]$/, '$1')
    return acc
  }, {})
  return { type, props }
}

const parseBody = (b) => { b = b.trim()
  if (!b || b == '--') return
  const { headers, body } = splitBody(b)

  const lh = getHeaderLines(headers)
  const parsedHeaders = getHeaders(lh)
  const ct = findHeader(parsedHeaders, 'content-type') || ''
  const encoding = findHeader(parsedHeaders, 'content-transfer-encoding')

  const { type, props } = getTypeAndProps(ct)

  const { charset, boundary } = props
  const bb = prepareBody(body, type, encoding, charset, boundary)

  return { headers, parsedHeaders, type, ...bb, encoding, ...props }
}

const prepareBody = (body, contentType, encoding, charset, boundary) => {
  if (contentType == 'multipart/related') {
    const parts = body.split(`--${boundary}`)
      .map(parseBody).filter(Boolean)
    return { parts }
  }
  if (contentType == 'text/plain') {
    return { plain: body }
  }
  if (contentType == 'text/html') {
    // let html
    // if (encoding == 'quoted-printable') {
    //   const unicode = /utf-8/i.test(charset)
    //   // console.log('decode', unicode)
    //   body = decode(body, unicode)
    // } else if (encoding == 'base64') {
    //   body = new Buffer(body, 'base64').toString(charset)
    // }
    return { html: body }
  }
  if (/^multipart\/(alternative|mixed)/i.test(contentType)) {
    if (!boundary) throw new Error('Boundary is required for multipart.')
    // console.log(boundary)
    // console.log(body)
    // body = body.replace(/\s*--\s*$/, '')
    const alts = body.split(`--${boundary}`)
      .map(parseBody).filter(Boolean)
    return { alts }
    // bodies[bodies.length - 1].body += '<img src="https://avatars0.githubusercontent.com/u/60678477?s=80&v=4" alt="testing">'
    // console.log(alts)
  }
  console.log('Unknown type???')
  return { data: body }
}

const getHeaderLines = (headers) => {
  let prev = ''
  let even = true
  const lines = headers.split(/(\r?\n)/).reduce((acc, current, i, a) => {
    even = !even
    if (even) return acc // separator at even
    const sep = a[i+1] || ''
    if (!prev || /^\s/.test(current)) {
      prev += current.replace(/^\s+/, '') + sep
    } else {
      acc.push(prev)
      prev = current
    }
    return acc
  }, [])
  if (prev) lines.push(prev)
  return lines
}

const findHeader = (headers, header) => {
  let [,v = ''] = Object.entries(headers).find(([key]) => {
    if (key.toLowerCase() == header.toLowerCase()) return true
  }) || []
  // https://dmorgan.info/posts/encoded-word-syntax/
  // if (header == 'from') console.log(v)
  // console.log(v)
  let needsRepl = false
  let sv = v.replace(/^=\?(.+?)\?(.+?)\?(.+?)\?=/gm, (m, encoding, charset, value) => {
    needsRepl = true
    // console.log('!', encoding, charset, value)
    if (/^b$/i.test(charset)) return new Buffer(value, 'base64').toString()
    if (/^q$/i.test(charset)) {
      value = decode(value, encoding.toUpperCase() == 'UTF-8')
      // console.log(value)
      return value.replace(/_/g, ' ')
    }
    return value
  })
  if (needsRepl) sv = sv.replace(/\r?\n/g, '')
  return sv
}
const getHeaders = (lines) => {
  // console.log(lines)
  const parsedHeaders = lines.reduce((acc, line) => {
    const [, header, value] = /(.+?):([\s\S]+)/.exec(line) || []
    if (!header) return acc
    acc[header] = value.trim()
    return acc
  }, {})
  return parsedHeaders
}

const splitBody = (s) => {
  try {
    const [, headers, ws] = s.split(/([\s\S]+?)(\r?\n\r?\n)/, 3)
    const offset = headers.length + ws.length
    const body = s.slice(offset)
    return { headers, body }
  } catch (err) {
    return { headers: '', body: s }
  }
}

// export const middleware = ['form']